;(function(public) {

	public.chatApp = public.chatApp || {};

	var dbUrl = "https://mm-chat.firebaseio.com/";
	var dbRef = new Firebase(dbUrl);

	chatApp.db = {
		url: dbUrl,
		ref: dbRef
	};

})(this);