;(function(public) {

	public.chatApp = public.chatApp || {};
	public.EVT = new EventEmitter2();

    var btns = document.querySelector('.btns');
	var logoutBtn = document.querySelector('#logout-btn');

	function logoutHandler(ev) {
		ev.preventDefault();
		chatApp.db.ref.unauth();
	}

	btns.addEventListener('click', function(ev) {
        chatApp.handleUser(ev.target.id.replace('-btn', ''));
	}, false);

	logoutBtn.addEventListener('click', logoutHandler, false);

	document.addEventListener('DOMContentLoaded', function() {
		EVT.emit('check-login');
	}, false);

})(this);
