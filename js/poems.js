;(function() {

	var poemsList = document.querySelector('.poems');

	function getPoems() {
		var poemsHTML = '';
		if(chatApp.userID){
			chatApp.db.user.on("child_added", function(snapshot) {
                var poem = snapshot.val();
                poemsHTML += '<div class="poem-container">' +
								'<h3 class="poem-title">' + poem.title + '</h3>' +
                                '<div class="poem-text">' + poem.text + '</div>' +
                              '</div>';
                poemsList.innerHTML = poemsHTML;
			});
	 	}else {
	 		poemsList.innerHTML = '';
	 	}
	}

	EVT.on('check-login', getPoems);

})();

