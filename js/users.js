;(function(public) {

	public.chatApp = public.chatApp || {};

	chatApp.userID = null;

	var msg = document.querySelector('.msg');
	var userPic = document.querySelector('.user-pic');
	var userName = document.querySelector('.user-name');
	var userBox = document.querySelector('.user-box');

	function handleUser(method) {
		var email = document.querySelector('#user-email').value;
		var pass = document.querySelector('#user-pass').value;
		if(method === 'create') {
			chatApp.db.ref.createUser({
              email    : email,
              password : pass
            }, function(error, userData) {
              if (error) {
                msg.classList.add('error');
                msg.textContent = "Error creating user: " + error;
                console.log("Error creating user:", error);
              } else {
                msg.classList.add('success');
                msg.textContent = "Successfully created user account for: " + email;
                console.log("Successfully created user account with uid:", userData.uid);
              }
            });
		}else if(method === 'login'){
			chatApp.db.ref.authWithPassword({
			  email    : email,
			  password : pass
			}, function(error, authData) {
			  if (error) {
                msg.classList.add('error');
                msg.textContent = "Login failed: " + error;
			    console.log("Login Failed!", error);
			  } else {
			    console.log("Authenticated successfully with payload:", authData);
			  }
			});
		}
        setTimeout(function (){
            msg.textContent = '';
        }, 4000);
	}

	chatApp.db.ref.onAuth(function(authData) {
	  if (authData) {
	  	userPic.src = authData.password.profileImageURL;
		userName.textContent = authData.password.email.replace(/@.*/, '');
		userBox.classList.remove('hidden');
		document.body.classList.add('user-logged');
		chatApp.userID = authData.uid;
		chatApp.db.user = new Firebase(chatApp.db.url + chatApp.userID + '/');
	  } else {
	    console.log("Client unauthenticated.");
	    userBox.classList.add('hidden');
	    document.body.classList.remove('user-logged');
        chatApp.userID = null;
		chatApp.db.user = null;
	  }
	  EVT.emit('check-login');
	});

	chatApp.handleUser = handleUser;

})(this);
