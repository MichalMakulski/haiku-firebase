;(function(public) {

	public.chatApp = public.chatApp || {};

	var poemEditor = document.querySelector('.poem-editor');

	var poemElts = '<label for="poem-title">Poem title:</label><br />' +
                    '<input id="poem-title" type="text" name="poem-title"><br />' +
					'<textarea id="poem-txt" rows="4" cols="50" placeholder="Write your haiku" name="poem-text"></textarea><br />' +
					'<button id="add-poem">ADD POEM</button>';

	function addPoem(ev) {
		if (chatApp.userID && ev.target.id === 'add-poem') {
			var poemTitle = document.querySelector('#poem-title').value;
			var poemTxt = tinyMCE.activeEditor.getContent();
            chatApp.db.user.push({
                title: poemTitle,
                text: poemTxt
            });
	    }
	}

	poemEditor.addEventListener('click', addPoem, false);

	EVT.on('check-login', function() {
		if(chatApp.userID) {
            poemEditor.innerHTML = poemElts;
            tinymce.init({
                selector: 'textarea',
                width:500
            });
		}else {
			poemEditor.innerHTML = '';
		}
	});


})(this);
